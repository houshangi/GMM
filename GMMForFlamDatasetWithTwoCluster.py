
import numpy as np
import matplotlib.pyplot as plt
from sklearn.mixture import GaussianMixture

X_train = np.loadtxt('a.txt')




gmm = GaussianMixture(n_components=2)
gmm.fit(X_train)

print(gmm.means_)
print('\n')
print(gmm.covariances_)

X, Y = np.meshgrid(np.linspace(-2.5,16), np.linspace(14,32))
XX = np.array([X.ravel(), Y.ravel()]).T
Zfor2clusters= gmm.score_samples(XX)
print "###################################"
print Zfor2clusters
print "######################################"
Z1 = Zfor2clusters.reshape((50,50))
plt.contour(X, Y, Z1)
plt.scatter(X_train[:, 0], X_train[:, 1])

plt.show()

def getZfor2clusters():
    return Zfor2clusters

