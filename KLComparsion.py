from KLImplementation import KL
from GMMForFlamDatasetWithTwoCluster import getZfor2clusters
from GMMForFlameDatasetWithFourCluster import getzfor4clusters
from GMMForFlameDatasetWithOneCluster import getzfor1clusters
from GMMForFlameDatasetWithThreeCluster import getzfor3clusters
from scipy import stats


print "*******************************"
print "KL compare between 2 and 4 "
print  KL(getZfor2clusters() , getzfor4clusters())
print "its estimetical "
print "*******************************"

print "*******************************"
print "KL compare between 1 and 2 "
print  KL(getzfor1clusters() , getZfor2clusters())
print "its estimetical "
print "*******************************"

print "*******************************"
print "KL compare between 2 and 3 "
print  KL(getZfor2clusters() , getzfor3clusters())
print "its estimetical "
print "*******************************"


print "*******************************"
print "KL compare between 1 and 4"
print  KL(getzfor1clusters() , getzfor4clusters())
print "its estimetical "
print "*******************************"





