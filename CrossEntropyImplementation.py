import numpy as np

def cross_entropy(predictions, targets, epsilon=0):
    predictions = np.clip(predictions, epsilon, 1. - epsilon)
    N = predictions.shape[0]
    ce = np.sum(np.sum(targets*np.log(predictions+1e-9)))/N
    return ce


