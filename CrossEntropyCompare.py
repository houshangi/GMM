from CrossEntropyImplementation import cross_entropy
from GMMForFlameDatasetWithThreeCluster import getzfor3clusters
from GMMForFlameDatasetWithOneCluster import getzfor1clusters
from GMMForFlamDatasetWithTwoCluster import getZfor2clusters
from  GMMForFlameDatasetWithFourCluster import getzfor4clusters
from KLImplementation import KL

#########################################################################################


CEbetween4and3 =cross_entropy(getzfor3clusters(),getzfor4clusters())
CEbetween3and2 = cross_entropy(getZfor2clusters(),getzfor3clusters())
CEbetween1and2 = cross_entropy(getzfor1clusters() , getZfor2clusters())

print CEbetween4and3
print CEbetween1and2
print CEbetween3and2

##########################################################################################
